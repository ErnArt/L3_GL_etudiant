"use strict";

function fiboIterative (n) {
	if (n < 2)
		return n;
	else {
		let f0 = 0;
		let f1 = 1;
		for (let i=1; i<n; i++) {
			const tmp = f1+f0;
			f0 = f1;
			f1 = tmp;
		}
		return f1;
	}
}

function repeatN (n, f)
{
	let arrayFibo = [];
	for (let i = 0; i < n; i++)
	{
		arrayFibo.push(f(i));
	}
	return arrayFibo;
}