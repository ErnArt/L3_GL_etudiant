# Drunk Player

## Description

Drunk Player est un système de lecture de vidéos qui a trop bu. Il lit les vidéos contenues dans un dossier par morceaux, aléatoirement et parfois en transformant l'image.

Drunk Player est composé :
- d'une bibliothèque (drunk_player) contenant le code de base
- d'un programme graphique (drunk_player_gui) qui affiche le résultat à l'écran
- d'un programme console (drunk_player_cli) qui sort le résultat dans un fichier

## Dépendances

- OpenCV
- Boost

## Compilation

```bash
mkdir build
cd build
cmake ..
make
```

## Utilisation

```bash
./drunk_player_gui.out ../**data**/
```

![ALT](drunk_player_gui.png)