#include "Fibo.hpp"
#include <iostream>

int main() 
{
	try
	{
		for (int i=0; i<50; i++)
			std::cout << fibo(i) << std::endl;
	}
	catch(std::string e)
	{
		std::cout<<"Error "<<e<<std::endl;
	}
	return 0;
}

