#include <cmath>

float tchaTchaSinus(float x, float a, float b)
{
	return sin(2*M_PI*(a*x+b));
}

#include <emscripten/bind.h>

EMSCRIPTEN_BINDINGS(tchaTchaSinus) {
	emscripten::function("tchaTchaSinus", &tchaTchaSinus);
}

