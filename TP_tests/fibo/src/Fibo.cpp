#include "Fibo.hpp"
#include <cassert>
#include <iostream>

int fibo(int n, int f0, int f1)
{
	if (f1 < f0)
	{
		throw std::string("f1 < 0");
	}
	if (f0 < 0)
	{
		throw std::string("f0 < 0");
	}
	return n<=0 ? f0 : fibo(n-1, f1, f1+f0);
}